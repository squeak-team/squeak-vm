squeak-vm (1:4.10.2.2614-4) unstable; urgency=medium

  * Fix use system shared config.guess and config.sub (not old local
    versions).
    Closes: bug#804164. Thanks to Jurica Stanojkovic.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 09 Nov 2015 10:24:52 +0100

squeak-vm (1:4.10.2.2614-3) unstable; urgency=medium

  * Gix stop build-depend on libgstreamer0.10-dev: Plugin isn't built
    anyway.
    Closes: Bug#799726. Thanks to Moritz Muehlenhoff and Tobias Pape.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 28 Oct 2015 17:34:33 +0100

squeak-vm (1:4.10.2.2614-2) unstable; urgency=medium

  * Update README.source to emphasize that control.in file is *not* a
    show-stopper for contributions.
  * Move packaging to pkg-squeak team governance.
  * Declare compliance with Debian Policy 3.9.6.
  * Improve short and long description, avoiding article in short
    description, and referencing Debian packages (not web pages) for
    Squeak images.
  * Modernize CDBS use:
    + Have utils.mk put aside cruft during build.
      Tighten to build-depend on recent cdbs.
    + Stop needlessly relax upstream-tarball.mk inclusion.
    + Include cmake.mk (bypassing upstream configure script): Enables
      fortifying options.
  * Fix menu file.
  * Recommend scratch with etoys as fallback (and stop suggest etoys).
    Closes: bug#603797. Thanks to Ronny Standtke.
  * Remove /usr/share/squeak dir when purging package.
    Closes: Bug#658108. Thanks to Andreas Beckmann.
  * Fix drop Education tag from desktop file.
    Closes: Bug#611115. Thanks to Chris Wilson.
  * Fix FTBFS with build-flag --as-needed.
    Closes: Bug#710367. Thanks to Julian Taylor.
  * Fix build-depend on libasound2-dev only on Linux archs, and
    build-exclude it elsewhere.
    Closes: Bug#696606. Thanks to Steven Chamberlain.
  * Fix disable FFIPrims plugin on powerpcspe.
    Closes: Bug#693633. Thanks to Roland Stigge.
  * Add patch to fix FTBFS with clang.
    Closes: bug#740972. Thanks to Nicolas Sévelin-Radiguet.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 21 Jul 2015 20:02:34 +0200

squeak-vm (1:4.10.2.2614-1.1) unstable; urgency=medium

  * Non-maintainer upload
  * Change B-D to libjpeg-dev to finish the transition to libjpeg-turbo
    (Closes: #763494)

 -- Ondřej Surý <ondrej@debian.org>  Mon, 29 Sep 2014 17:06:04 +0200

squeak-vm (1:4.10.2.2614-1) unstable; urgency=low

  [ upstream ]
  * New upstream release.
    Changes since last packaged release:
    + Support building and installing VMs for 64-bit images.
    + UUIDPlugin can use uuid_create().
    + Link squeakvm against libiconv if necessary.
    + Search /usr/local and /usr/pkg for headers and libs.
    + Rename "aio.h" -> "sqaio.h".
    + Allow the interpreter VM to execute format 6505 images.
    + More robust check for freetype2 headers.
    + Allow a headless image to change the size of the Display.
    + Compute gmt offset when tm_gmtoff not available on Solaris.
    + Interpreter source updated to VMMaker 4.10.2.
    + Fixes to build process for compatibility with most recent
      subversion client and bizarre situations with uuid.h on some Linux
      distributions.
    + Obsolete autotools files removed.
    + New plugins for Scratch support: WeDoPlugin, CameraPlugin,
      ScratchPlugin.
    + Trailing NUL not included when copying strings into Squeak via
      locale.
    + Re-enable gcc optimisations for fdlibm (FloatMathPlugin).
    + SerialPlugin ensures descriptor close and enforces limit on max
      open ports.
    + UUIDPlugin refuses to load if uuid_generate() would cause a
      segmentation fault.
    + Mpeg3Plugin never uses pthreads.
    + Main window clips by children for the convenience of OpenGL
      subwindows.

  [ Jonas Smedegaard ]
  * Add watch file.
  * Add control file Vcs-* fields: Packaging moved to Git.
  * Add get-orig-source target, using CDBS upstream-tarball.mk snippet.
  * Add myself as uploader.
  * Stop including or build-depending on quilt (patching handled by dpkg
    with source version 3.0).
  * Drop patch to reenable-gcc-optimisations: Applied upstream.
  * Unfuzz/refresh patches with shortening quilt options.
  * Fix avoid removing files added by linex patch.
  * Ease git-buildpackage integration:
    + Git-ignore quilt .pc dir.
    + Enable use of signed tags and pristine-tar.
  * Put aside convenience code copies during build (not remove them as
    done in NMU: missing files complicates building with
    git-buildpackage).
  * Rename patch to avoid embedded libraries, and improve its DEP3
    header.
  * Simplify packaging, using CDBS.
  * Add patch fix_printf_syntax to improve printf syntax: Fails with
    hardened build options.
  * Relax to build-depend unversioned on debhelper and cmake: Needed
    versions satisfied in stable, and oldstable no longer supported.
  * Bump debhelper compatibility level to 8.
  * Update README.source: Replace obsolete dpatch note with references
    to CDBS and git-buildpackage.
  * Fix respect nostrip DEB_BUILD_OPTIONS option.
  * Merge linex patch into debian packaging.
  * Bump standards-version to 3.9.4.
  * Build-depend on dh-buildinfo, to help debug independently from
    central Debian resources.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 25 Dec 2012 18:37:50 +0100

squeak-vm (1:4.4.7.2357-1.1) unstable; urgency=low

  [ Hector Oron ]
  * Non-maintainer upload.
  * Fix FTBFS (arm): unrecognized command line option '-mno-fused-madd'
    - Thanks Ricardo Salveti de Araujo for patch. (Closes: #634240)

  [ Neil Williams ]
  * Adapt CMake to not build embedded libraries.

 -- Neil Williams <codehelp@debian.org>  Sat, 03 Mar 2012 17:32:01 +0000

squeak-vm (1:4.4.7.2357-1) unstable; urgency=low

  * New upstream release (Closes: #625861)
  * debian/control:
     - bumped standards version to 3.9.2  
     - fixed suggestions in debian/control (Closes: #528156)
  * Disabled npsqueak patch, as browser plugin doesn't compile

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Sat, 07 May 2011 11:18:30 +0200

squeak-vm (1:4.0.3.2202-2) unstable; urgency=low

  * Fix error in debian/rules

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Sat, 17 Apr 2010 20:17:57 +0200

squeak-vm (1:4.0.3.2202-1) unstable; urgency=low

  * New upstream release (Closes: #533845)
  * Enabled mpeg plugin only on i386 arch (Closes: #567366)
  * Switch to dpkg-source 3.0 (quilt) format  
  * debian/control:
     - bumped standards version to 3.8.4
     - added quilt build dependency
     - changes dependency from xterm to x-terminal-emulator
     (Closes: #498782)
  * debian/rules: modified to use quilt  
  * Added epoch field to help collaborative mainteinance with other
    Debian based distributions (Closes: #576444)
  * removed sound64bits.dpatch as it's been included upstream
  * debian/patches/mpeg_includes.patch for missing includes (Thanks to 
    Petter Petter Reinholdtsen at #567366)
    
 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Thu, 15 Apr 2010 12:14:14 +0200

squeak-vm (3.11.3+svn2147-1) unstable; urgency=low

  * New upstream release, with new RomePlugin and GstreamerPlugin, 
    pulseaudio support and use cmake to compile (Closes: #552907)
  * Links every sources file to the user squeak directory
  * Fixes plugin links
  * Removed 20_sym_link_patch and fixImplicitidConvertedPointer.dpatch 
    patches as they're included upstream
  * Added sound64bits.dpatch (Closes: #487871)
  * Reintroduced MPEG3Plugin as it uses the same sources and headers the
    current libmpeg3 package in Debian
  * xresetcapslock removed as the vm now doesn't interprets CAPS LOCK as
    SHIFT being down
  * Removed browser plugin as it is not maintained upstream by now
  * debian/postinst: squeaklets directory now points to /tmp (Closes: #488134)
  * linex.dpatch: fixes images detection and desktop file (Closes: #496559)
  * Bumped standards version to 3.8.3: added debian/README.source
  * Disabled RomePlugin for all archs except i386

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Sun, 27 Dec 2009 16:16:46 +0100

squeak-vm (3.10.3+svn1902.dfsg-1) unstable; urgency=low

  * New upstream release (Include new plugins for ogg, gstreamer & dbus)
  * Following patches have been removed as they are applied upstream:
    - 10_configure
    - 15_biasToGrow
    - 64bits_SocketPlugin.dpatch
  * disabled 30_window_icon as it does not work correctly once compiled
  * debian/control: 
    - new build dependencies to compile new plugins: libdbus-1-dev,
      libgstreamer0.10-dev, libvorbis-dev
    - added autotools-dev as build dependency to regenerate config.sub 
      and config.guess files
    - Bumped standards-version to 3.8.0 (no changes required)
    
 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Tue, 17 Jun 2008 10:47:30 +0200

squeak-vm (3.9.12+svn1820.dfsg-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Build depend on libffi-dev instead of libffi4-dev. Closes: #479712.

 -- Frans Pop <fjp@debian.org>  Fri, 09 May 2008 23:38:57 +0200

squeak-vm (3.9.12+svn1820.dfsg-2) unstable; urgency=low

  * Updated squeak launcher man page (Closes: #471436)
  * Fixed launcher error and changed its interpreter (Closes: #472220)
  * Created fixImplicitidConvertedPointer.dpatch (Closes: #471272)

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Sat, 22 Mar 2008 23:04:03 +0100

squeak-vm (3.9.12+svn1820.dfsg-1) unstable; urgency=low

  * First official Debian release (Closes: #454635).
  * Updated libtool files
  * Removed MPEG3Plugin as even it's LGPL licensed contains code under
    some evil patents.
  * Following patches have been applied to the upstream svn version
    - 10_configure
    - 15_biasToGrow: biasToGrow code typo (Squeak #6667)
    - 20_sym_link_patch: fix symlinks on Linux
    - 30_window_icon: add an icon to the Squeak window (by Matej Kosik)
    - 50_defaultSourcesSystemAttribute: set default place for sources
    - npsqueak: modify plugin launcher to work in Debian 
    - 64bits_SocketPlugin: fix a segfault in the 64 bits (Squeak #5688)
    - linex: legacy files from previous Squeak package in gnuLinEx 
  * platforms/Cross/plugins/JPEGReadWriter2Plugin/README added as it
    was lost in the svn and contains license text for this plugin
  * Added launcher desktop friendly, desktop integration and icons

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Wed, 05 Mar 2008 19:54:53 +0100
