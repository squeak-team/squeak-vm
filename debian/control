Source: squeak-vm
Section: interpreters
Priority: optional
Maintainer: Debian Squeak Team <pkg-squeak-devel@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>,
 José L. Redrejo Rodríguez <jredrejo@debian.org>
Build-Depends: cdbs (>= 0.4.106~),
 autotools-dev,
 debhelper,
 dh-buildinfo,
 cmake,
 libxt-dev,
 libgl1-mesa-dev,
 libasound2-dev [linux-any],
 uuid-dev,
 libspeex-dev,
 libxtst-dev,
 libxrender-dev,
 sharutils,
 libffi-dev,
 libdbus-1-dev,
 libvorbis-dev,
 libfreetype6-dev,
 libpango1.0-dev,
 libcairo2-dev,
 libpulse-dev,
 libjpeg-dev,
 libpcre3-dev
Build-Conflicts: libasound2-dev [!linux-any]
Standards-Version: 3.9.6
Homepage: http://www.squeakvm.org/unix/
Vcs-Git: git://anonscm.debian.org/pkg-squeak/squeak-vm
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-squeak/squeak-vm.git

Package: squeak-vm
Architecture: any
Depends: ${shlibs:Depends},
 ${misc:Depends},
 whiptail,
 xterm | x-terminal-emulator,
 gettext-base
Recommends: zenity | kdebase-bin, scratch | etoys
Description: Unix Squeak virtual machine
 Squeak is a full-featured implementation of the Smalltalk programming
 language and environment based on (and largely compatible with) the
 original Smalltalk-80 system.
 .
 This package contains just the Unix Squeak virtual machine.  You will
 likely need also an image file containing a "snapshot" of a live Squeak
 session - e.g. one of the Debian packages etoys or scratch.
